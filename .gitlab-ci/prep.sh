#!/bin/bash

set -x
set -e

HELM_VERSION=v3.7.0
KUSTOMIZE_VERSION=v4.4.0
HELMFILE_VERSION=v0.141.0
KUBECTL_VERSION=v1.24.1

curl -LO https://get.helm.sh/helm-${HELM_VERSION}-linux-amd64.tar.gz
tar xvf helm-${HELM_VERSION}-linux-amd64.tar.gz -C /usr/local/bin --strip-components 1 linux-amd64/helm
curl -LO https://github.com/kubernetes-sigs/kustomize/releases/download/kustomize%2F${KUSTOMIZE_VERSION}/kustomize_${KUSTOMIZE_VERSION}_linux_amd64.tar.gz
tar xvf kustomize_${KUSTOMIZE_VERSION}_linux_amd64.tar.gz -C /usr/local/bin kustomize
helm plugin install https://github.com/databus23/helm-diff --version master
curl -L -o /usr/local/bin/helmfile https://github.com/roboll/helmfile/releases/download/${HELMFILE_VERSION}/helmfile_linux_amd64
chmod +x /usr/local/bin/helmfile

helm repo add gitlab https://charts.gitlab.io

curl -LO "https://dl.k8s.io/release/${KUBECTL_VERSION}/bin/linux/amd64/kubectl"
install -o root -g root -m 0755 kubectl /usr/local/bin/kubectl
rm kubectl

pip install yq
