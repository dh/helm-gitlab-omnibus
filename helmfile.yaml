repositories:
- name: gitlab
  url: https://charts.gitlab.io
- name: grafana
  url: https://grafana.github.io/helm-charts
- name: twuni # for registry
  url: https://helm.twun.io
- name: ingress-nginx
  url: https://kubernetes.github.io/ingress-nginx
- name: bitnami
  url: https://charts.bitnami.com/bitnami

environments:
  lint:
    values:
    - releaseName: lint
    - namespace: default
    - externalGrafana: true
  test:
    values:
    - releaseName: test
    - namespace: gitlab
    - externalGrafana: true
  packet:
    values:
    - releaseName: prod
    - namespace: gitlab
    - externalGrafana: true
  packet-HA:
    values:
    - releaseName: prod
    - namespace: gitlab
    - externalGrafana: true

templates:
  defaults: &defaults
    namespace: {{ .Values.namespace }}
    createNamespace: true
    missingFileHandler: Warn
    values:
      - configs/common/globals.gotmpl
        # configs/common/{gitlab|gitlab-omnibus|freedesktop|...}.gotmpl
      - configs/common/{{`{{ trimSuffix "-" (trimSuffix .Values.releaseName .Release.Name) }}`}}.gotmpl
        # configs/{gke|test|...}/globals.gotmpl
      - configs/{{`{{ .Environment.Name }}`}}/globals.gotmpl
        # configs/{gke|test|...}/{gitlab|gitlab-omnibus|freedesktop|...}.gotmpl
      - configs/{{`{{ .Environment.Name }}`}}/{{`{{ trimSuffix "-" (trimSuffix .Values.releaseName .Release.Name) }}`}}.gotmpl
        # secrets/{gke|test|...}/globals.gotmpl
      - secrets/{{`{{ .Environment.Name }}`}}/globals.gotmpl
        # secrets/{gke|test|...}/{gitlab|gitlab-omnibus|freedesktop|...}.gotmpl
      - secrets/{{`{{ .Environment.Name }}`}}/{{`{{ trimSuffix "-" (trimSuffix .Values.releaseName .Release.Name) }}`}}.gotmpl
  backups: &backups
    installed: {{ not (eq .Environment.Name "lint" "test") }}
    chart: gitlab/gitlab
    version: {{ readFile "gitlabVersion" }}
    namespace: gitlab
    <<: *defaults
    values:
      - configs/common/globals.gotmpl
        # configs/common/{gitlab|gitlab-omnibus|freedesktop|...}.gotmpl
      - configs/common/{{`{{ trimSuffix "-" (trimSuffix .Values.releaseName .Release.Name) }}`}}.gotmpl
        # configs/{gke|test|...}/globals.gotmpl
      - configs/{{`{{ .Environment.Name }}`}}/globals.gotmpl
        # next one is put after the last global definitions
      - configs/common/gitlab-backup.gotmpl
      - configs/{{`{{ .Environment.Name }}`}}/gitlab-backup.gotmpl
        # configs/{gke|test|...}/{gitlab|gitlab-omnibus|freedesktop|...}.gotmpl
      - configs/{{`{{ .Environment.Name }}`}}/{{`{{ trimSuffix "-" (trimSuffix .Values.releaseName .Release.Name) }}`}}.gotmpl
        # secrets/{gke|test|...}/globals.gotmpl
      - secrets/{{`{{ .Environment.Name }}`}}/globals.gotmpl
        # secrets/{gke|test|...}/{gitlab|gitlab-omnibus|freedesktop|...}.gotmpl
      - secrets/{{`{{ .Environment.Name }}`}}/{{`{{ trimSuffix "-" (trimSuffix .Values.releaseName .Release.Name) }}`}}.gotmpl


releases:
  - name: gitlab-{{ .Values.releaseName }}
    installed: {{ not (eq .Environment.Name "lint") }}
    chart: gitlab/gitlab
    version: {{ readFile "gitlabVersion" }}
    <<: *defaults
  - name: gitlab-backup-db-{{ .Values.releaseName }}
    <<: *backups
  - name: gitlab-backup-packages-{{ .Values.releaseName }}
    <<: *backups
  - name: gitlab-backup-pages-{{ .Values.releaseName }}
    <<: *backups
  - name: freedesktop-{{ .Values.releaseName }}
    installed: true
    chart: ./charts/freedesktop
    <<: *defaults
  - name: grafana-{{ .Values.releaseName }}
    installed: {{ and (.Values | get "externalGrafana" false) (not (eq .Environment.Name "lint" "packet")) }}
    version: 6.18.2
    chart: grafana/grafana
    <<: *defaults
  - name: fdo-grafana-{{ .Values.releaseName }}
    installed: {{ eq .Environment.Name "packet-HA"}}
    chart: ./charts/fdo-grafana
    <<: *defaults
  - name: fdo-gitlab-ingress-nginx-{{ .Values.releaseName }}
    installed: {{ eq .Environment.Name "packet-HA"}}
    chart: ingress-nginx/ingress-nginx
    version: 4.0.6
    <<: *defaults
    namespace: gitlab
  - name: fdo-approve-users-{{ .Values.releaseName }}
    installed: {{ eq .Environment.Name "packet-HA" }}
    chart: ./charts/fdo-approve-users
    <<: *defaults
    namespace: gitlab
  - name: fdo-registry-cache-{{ .Values.releaseName }}
    installed: {{ eq .Environment.Name "packet-HA" }}
    chart: ./charts/fdo-registry-cache
    <<: *defaults
    namespace: registry-cache
  - name: fdo-ingress-nginx-{{ .Values.releaseName }}
    installed: {{ eq .Environment.Name "packet-HA"}}
    chart: ingress-nginx/ingress-nginx
    version: 4.0.6
    <<: *defaults
    namespace: nginx
  - name: fdo-opa-{{ .Values.releaseName }}
    installed: {{ eq .Environment.Name "packet-HA" "lint"}}
    chart: ./charts/fdo-opa
    <<: *defaults
    namespace: opa-istio
  - name: minio-etcd-{{ .Values.releaseName }}
    installed: false #{{ eq .Environment.Name "packet-HA" }}
    chart: bitnami/etcd
    version: 6.13.9
    <<: *defaults

